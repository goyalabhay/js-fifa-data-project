const WorldCupMatches = "./src/data/WorldCupMatches.csv";
const WorldCups = "./src/data/WorldCups.csv";
const WorldCupPlayers = "./src/data/WorldCupPlayers.csv";

const matchesPlayedPerCity = require("./src/server/matchesPlayedPerCity.cjs");
const matchesWonPerTeam = require("./src/server/matchesWonPerTeam.cjs");
const redCardsIssued = require("./src/server/redCardsIssued.cjs");
const top10Players = require("./src/server/top10Players.cjs");

const csv = require("csvtojson");
const fs = require("fs");

// Problem 1 (Number of matches played per city)
csv()
  .fromFile(WorldCupMatches)
  .then((WorldCupMatches) => {
    const jsonString = JSON.stringify(matchesPlayedPerCity(WorldCupMatches));
    fs.writeFile(
      "./src/public/output/matchesPlayedPerCity.json",
      jsonString,
      (err) => {
        if (err) {
          throw Error;
        }
      }
    );
  });

// Problem 2 (Number of matches won per team)
csv()
  .fromFile(WorldCups)
  .then((WorldCups) => {
    const jsonString = JSON.stringify(matchesWonPerTeam(WorldCups));
    fs.writeFile(
      "./src/public/output/matchesWonPerTeam.json",
      jsonString,
      (err) => {
        if (err) {
          throw Error;
        }
      }
    );
  });

// Problem 3 (Find the number of red cards issued per team in 2014 World Cup)

let bothData = [];
let path = [WorldCupMatches, WorldCupPlayers];

for (let p of path) {
  csv()
    .fromFile(p)
    .then((data) => {
      bothData.push(data);
      if (bothData.length === path.length) {
        const jsonString = JSON.stringify(
          redCardsIssued(bothData[0], bothData[1], "2014")
        );
        fs.writeFile(
          "./src/public/output/redCardsIssued.json",
          jsonString,
          (err) => {
            if (err) {
              throw Error;
            }
          }
        );
      }
    });
}

// Problem 4 (Find the top 10 players with the highest probability of scoring a goal in a match)
csv()
  .fromFile(WorldCupPlayers)
  .then((WorldCupPlayers) => {
    const jsonString = JSON.stringify(top10Players(WorldCupPlayers));
    fs.writeFile("./src/public/output/top10Players.json", jsonString, (err) => {
      if (err) {
        throw Error;
      }
    });
  });
