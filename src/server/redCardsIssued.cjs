let redCardsIssued = (WorldCupMatches, WorldCupPlayers, year) => {
  let matches = WorldCupMatches.filter((match) => match["Year"] === year);

  let MatchIDSet = new Set(matches.map((match) => match["MatchID"]));

  let result = WorldCupPlayers.reduce((ans, player) => {
    let MatchID = player["MatchID"];
    if (MatchIDSet.has(MatchID) && player["Event"].includes("R")) {
      let teamInitial = player["Team Initials"];
      ans[teamInitial] = (ans[teamInitial] || 0) + 1;
    }
    return ans;
  }, {});

  return result;
};

module.exports = redCardsIssued;
