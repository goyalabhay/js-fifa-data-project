let matchesPlayedPerCity = (WorldCupMatches) => {
  return WorldCupMatches.reduce((ans, match) => {
    let city = match["City"].trim();
    if (city && city !== "") {
      ans[city] = (ans[city] || 0) + 1;
    }
    return ans;
  }, {});
};

module.exports = matchesPlayedPerCity;
