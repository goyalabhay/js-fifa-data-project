let matchesWonPerTeam = (WorldCups) => {
  return WorldCups.reduce((ans, match) => {
    let winner = match["Winner"].trim();
    if (winner && winner !== "") {
      ans[winner] = (ans[winner] || 0) + 1;
    }
    return ans;
  }, {});
};

module.exports = matchesWonPerTeam;
