let top10Players = (WorldCupPlayers) => {
  const goals = WorldCupPlayers.reduce((acc, curr) => {
    const player = curr["Player Name"];
    const event = curr["Event"];
    if (event.includes("G")) {
      const totalGoals = event.split("G").length - 1;
      acc[player] = (acc[player] || 0) + totalGoals;
    }
    return acc;
  }, {});

  const matches = WorldCupPlayers.reduce((acc, curr) => {
    const player = curr["Player Name"];
    acc[player] = (acc[player] || 0) + 1;
    return acc;
  }, {});

  const pr = Object.keys(goals).reduce((acc, player) => {
    if (matches[player]) {
      acc[player] = goals[player] / matches[player];
    }
    return acc;
  }, {});

  const players = Object.entries(pr)
    .sort(([, a], [, b]) => b - a)
    .slice(0, 10);

  const ans = Object.fromEntries(players);

  return ans;
};

module.exports = top10Players;
